; PRACTICA 1 - ARQUITECTURA Y ORGANIZACION DE COMPUTADORAS - G.Ing.Informatica
; SAMUEL ALFAGEME SAINZ - L1 - CURSO 2013/2014

segment .data
    prompt1 db "PR1 - AOC : Multiplicacion de 2 numeros enteros",10
    lpr1 equ $-prompt1
    prompt2 db "Introduzca el multiplicando: "
    lpr2 equ $-prompt2
    prompt3 db "Introduzca el multiplicador: "
    lpr3 equ $-prompt3
    prompt4 db " x "
    lpr4 equ $-prompt4
    prompt5 db " = "
    lpr5 equ $-prompt5
    saltolin db 10
    lslin equ $-saltolin
    parI    db "("
    lparI equ $-parI
    parD    db ")"
    lparD equ $-parD
segment .bss
    ; Los numeros se guardaran en 2 variables de tipo entero (4 bytes)
    numero1 resb 4 
    numero2 resb 4 
    resultado resb 4 ; El resultado de su multiplicacion sera otro numero entero 
segment .text
    global _start
    extern teclado    ; Carga de las funciones necesarias para las op. de E/S
    extern pantalla
_start:
    ; Impresion del mensaje inicial
    mov eax, 4
    mov ebx, 1
    mov ecx, prompt1
    mov edx, lpr1
    int 80h

    ; MULTIPLICANDO:
    ; Peticion:
    mov eax, 4
    mov ebx, 1
    mov ecx, prompt2
    mov edx, lpr2
    int 80h
    ; Recogida:
    mov eax, numero1 ; Paso de argumento por pila : direccion numero1 a la pila:
    ; La arquitectura de la MV. es de 64 bits y su pila es consecuente (rax -> registro de 64 bits)
    push rax
    call teclado     ; Llamada a la funcion teclado
    add rsp,8        ; recuperar el valor del apuntador a pila tras la llamada
    ; MULTIPLICADOR:
    ; Peticion:
    mov eax, 4
    mov ebx, 1
    mov ecx, prompt3
    mov edx, lpr3
    int 80h
    ; Recogida:
    mov eax, numero2
    push rax
    call teclado
    add rsp,8

    ; IMPRESION:
    ; Primer numero
    mov eax,[numero1]
    push rax
    call pantalla
    add rsp,8
    ; Imprimir el " x "
    mov eax, 4
    mov ebx, 1
    mov ecx, prompt4
    mov edx, lpr4
    int 80h
    ; Segundo numero
    ; en caso de ser negativo, evitamos el solapamiento de simbolos "x" y "-",
    ; encerrando el multiplicador entre parentesis:
    cmp dword[numero2],0
    jl parIz
    num2:
    mov eax,[numero2]
    push rax
    call pantalla
    add rsp,8
    cmp dword[numero2],0
    jl parDe
    multip:
    ; Operacion de multiplicacion:
    mov eax,[numero1]
    imul eax,[numero2]
    mov dword[resultado],eax 
    ; Imprimir el " = "
    mov eax, 4
    mov ebx, 1
    mov ecx, prompt5
    mov edx, lpr5
    int 80h
    ; Imprimir el resultado
    mov eax,[resultado]
    push rax
    call pantalla
    add rsp,8
    ; Imprimir un salto de linea
    mov eax, 4
    mov ebx, 1
    mov ecx, saltolin
    mov edx, lslin
    int 80h
    ; Fin del programa:
    mov eax, 1
    mov ebx, 0
    int 80h
    ; Funciones auxiliares en caso de necesitar parentesis
    parIz:
        mov eax, 4
        mov ebx, 1
        mov ecx, parI
        mov edx, lparI
        int 80h
        jmp num2
    parDe:
        mov eax, 4
        mov ebx, 1
        mov ecx, parD
        mov edx, lparD
        int 80h
        jmp multip