PR1 - ARQUITECTURA Y ORGANIZACIÓN DE COMPUTADORES
=================================================
Descripción:
------------
Desarrollo de un programa en ensamblador para IA-32 que realice un cálculo sencillo a partir de 2 argumentos enteros introducidos por el usuario y muestre su resultado por pantalla.

### Técnicas empleadas:
* Funciones de entrada y salida: manejo mediante la **interrupción 80h**.
*  Estructuración de un programa en subrutinas. 

Requisitos:
-----------
* [NASM]: the Netwide Assembler.

Ejemplo de ejecución:
---------------------
### Compilación:
```
nasm -f elf64 PR1.asm
nasm -f elf64 teclado.asm
nasm -f elf64 pantalla.asm
```
### Enlazado:
```
ld -o PR1 PR1.o teclado.o pantalla.o 
```
### Ejecución:
```
./PR1
PR1 - AOC : Multiplicacion de 2 numeros enteros Introduzca el multiplicando: 345
Introduzca el multiplicador: -2 
345 x (-2) = -690
```
Licencia:
---------
**Samuel Alfageme Sainz**

*Práctica 1 - Arquitectura y Organización de Computadoras*

*Grado en Ingeniería Informática - UVa*

MIT.

[NASM]:http://www.nasm.us/